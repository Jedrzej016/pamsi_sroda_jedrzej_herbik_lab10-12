#include<iostream>
#include <string>
#include <cstdlib>
#include <ctime>
#include <cmath>
#include <unistd.h>
#include "graf.hh"
#include "kolejka.hh"

using namespace std;

int dl_kolejki=0;

template <class Object,class Connect>
void screen(graf<Object,Connect>& graf,string kolej); // wyswietla plansze. string kolej pozwala na wyswietlenie dowolnego napisu obok planszy

bool sprawdz(int **tab,int rozm,int ile,int win); // funkcja zwraca bool jezeli wskazany grac spelnia warunek by wygrac

int gra(int wysokosc,int iloscDoWygranej); // funkcja gdzie jest zaimplementowana gra a za jej parametr podajemy rozmiar planszy i do ilu gramy

int main(int argc, char *argv[]){ // funkcja main zawiera menu
  int rozmiar=3; // rozmiar planszy
  int doIlu=3; // ile trzeba miec znakow w rzedzie by wygrac
  int tmp;
  char wybor; // zmienna pomocnicza do menu
  cout<<"Witaj"<<endl;
  cout<<"Oto gra w kolko i krzyzyk."<<endl;
  cout<<"autor: Jedrzej Herbik"<<endl;
  while(1){
    cout<<endl<<"Aktualne ustawienia: "<<endl;
    cout<<"rozmiar: "<<rozmiar<<endl;
    cout<<"ilosc pol wygrywajacych: "<<doIlu<<endl;
    cout<<"Menu:"<<endl;
    cout<<"q - wyjscie"<<endl;
    cout<<"p - gra"<<endl;
    cout<<"r - ustawienia rozmiaru planszy"<<endl;
    cout<<"w - ustawienie ilosci pol wygrywajacych"<<endl;
    cin>>wybor;
    switch(wybor){
    case 'q':
      system("clear");
      return 0;
      break;
    case 'p':
      system("clear");
      gra(rozmiar,doIlu);
      break;
    case 'r':
      system("clear");
      cout<<"Podaj rozmiar:";
      cin>>tmp;
      if((tmp<3)||(tmp>27)){ // jedyne ograniczenia rozmiaru planszy
	system("clear");
	cout<<endl<<"Rozmiar musi szię zawierac w przedziale 3-27"<<endl<<endl;
      }else{
	rozmiar=tmp;
	doIlu=tmp;     
	//	if(doIlu>rozmiar)doIlu=rozmiar;
	system("clear");
      }
      break;
    case 'w':
      system("clear");
      cout<<"Podaj rozmiar:";
      cin>>tmp;
      if((tmp>rozmiar)||(tmp<3)){ // ograniczenia co do wygrania
	cout<<endl<<"Ten parametr nie moze byc wiekrzy od rozmiaru planszy, ani mniejszy niz 3"<<endl<<endl;
      }else{
	doIlu=tmp;     
	system("clear");
      }
      break;
    default:
      system("clear");
      cout<<endl<<"Zleokreslony wybor"<<endl<<endl;
      break;
    }
  }
}

int gra(int wysokosc,int iloscDoWygranej){
  graf<char,int> graf; // graf na ktorym jest zbudowana gra
  int **winer=NULL; // macierz do okreslania czy ktos wygral
  int tmp;
  int ktoryRuch=0; // przechowuje informacje ile odbylo sie ruchow
  int wiersz,kolumna;
  char wybor[3]; // przechowuje informacje i tym ktore pole wybieramy
  bool whilebool=true; // zmienna pomocnicza do petli w funkcji while
  winer=new int* [wysokosc];
  for(int i=0;i<wysokosc;i++){
    winer[i]=new int [wysokosc];
  }  
  for(int i=0;i<wysokosc;i++){
    for(int j=0;j<wysokosc;j++){
      graf.insertVertex(' ');
      winer[i][j]=0;
    }
  }                               // tworzenie planszy i macierzy winner
  while(1){  // czesc gry
    while(whilebool){
      system("clear");
      screen(graf,"Twoja kolej");
      cout<<"    Jaki jest twoj ruch? ";
      cin>>wybor;
      if((wybor[0]=='e')&&(wybor[1]=='s')&&(wybor[2]=='c')){ // opcja zamkniecia gry
	for(int i=0;i<wysokosc;i++){
	  delete (winer[i]);
	}
	delete(winer);
	return 0;
      }
      if((wybor[0]>='0')&&(wybor[0]<=('0'+wysokosc))){ // jezei pierwszy znak wybor jest cyfra
	if((wybor[1]>='0')&&(wybor[1]<=('0'+wysokosc))){ // sprawdza czy drugi tez jest cyfra
	  kolumna=(wybor[0]-'0')*10+(wybor[1]-'0'); // jezeli tak to z fwuch pieerwszych znakow mamy nr kolumny
	  wiersz=wybor[2]-'a'; // z trzeciego znaku mamy znak wiersza
	}else{
	  kolumna=wybor[0]-'0'; // jezeli nie to z pierwszej cyfry mamy nr kolumny
	  wiersz=wybor[1]-'a'; // z drugiego znaku mamy znak wiersza
	}
	kolumna--; // tablice sa numerowane od 0 a w grze przyjalem oznaczenie od 1 wiec --
      }
      else if((wybor[1]>='0')&&(wybor[1]<=('0'+wysokosc))){ // podobna syuacja tylko znak jest na pierwszej pozycji a liczba na drugiej lub drugiej i trzeciej
	if((wybor[2]>='0')&&(wybor[2]<=('0'+wysokosc))){
	  kolumna=(wybor[1]-'0')*10+(wybor[2]-'0');
	  wiersz=wybor[0]-'a';
	}else{
	  kolumna=wybor[1]-'0';
	  wiersz=wybor[0]-'a';
	}
	kolumna--;
      }
      whilebool=false; // z tym warunkiem oposzczamy while
      if((wiersz<0)||(wiersz>=wysokosc)){ // jezeli blad to zmieniamy warunek i wybieramy pole ponownie
	cout<<"Zle okreslony wybor wiersza. ";
	whilebool=true;
      }
      if((kolumna<0)||(kolumna>=wysokosc)){ // jezeli blad to zmieniamy warunek i wybieramy pole ponownie
	cout<<"Zle okreslony wybor kolumny. ";
	whilebool=true;
      }
      if(whilebool==false){ // wybralismy zajete to zmieniamy warunek i wybieramy pole ponownie
	if(graf[wysokosc*wiersz+kolumna].getZawartosc()!=' '){
	  cout<<"To pole jest już zajete. ";
	  whilebool=true;	
	}
      }
    } // wybrano pole prawidlowo
    graf[wysokosc*wiersz+kolumna].getZawartosc()='x'; // wstawienie x w wybrane pole
    winer[kolumna][wiersz]=1; // odznaczenie tego w tablicy sprawdzajacej wygrane
    if(sprawdz(winer,wysokosc,iloscDoWygranej,1)){ // jezeli wygrales to konczymy gre
      system("clear");
      screen(graf,"Wygrales"); // powiadomienie o wygranej
      for(int i=0;i<wysokosc;i++){ // czyszczenie tablicy
	delete (winer[i]);
      }
      delete(winer);
      return 0;
    }
    ktoryRuch++; // zaznacza ze wykonano ruch
    if(ktoryRuch==graf.getListV().rozmiar()){ // jezeli wykonano wszystkie mozliwe ruchy to remis
      system("clear");
      screen(graf,"Remis");
      for(int i=0;i<wysokosc;i++){
	delete (winer[i]);
      }
      delete(winer);
      return 0;
    }
    whilebool=true;
    system("clear");
    screen(graf,"Kolej komputera");
    while(whilebool){
      whilebool=false;
      tmp=rand()%(wysokosc*wysokosc); // pierwsze pole losowo
      if(graf[tmp].getZawartosc()!=' '){ // jezeli wybrano puste pole to zakoncz petle
	whilebool=true;	
      }
    }
    whilebool=true;
    cout<<endl;
    graf[tmp].getZawartosc()='o'; // wstawienie o do pola
    winer[tmp%wysokosc][tmp/wysokosc]=2; // zaznaczenie w tablicy sprawdzajacej wygrana
    ktoryRuch++; // zaznaczenie kolejnego ruchu
    if(sprawdz(winer,wysokosc,iloscDoWygranej,2)){ // sprawdzenie czy przegrales
      system("clear");
      screen(graf,"Przegrales");
      for(int i=0;i<wysokosc;i++){
	delete (winer[i]);
      }
      delete(winer);
      return 0;
    }
    if(ktoryRuch==graf.getListV().rozmiar()){ // jezeli wykonano wszystkie ozliwe ruchy
      system("clear");
      screen(graf,"Remis");
      for(int i=0;i<wysokosc;i++){
	delete (winer[i]);
      }
      delete(winer);
      return 0;
    }
    sleep(1);
  }
}

template <class Object,class Connect>
void screen(graf<Object,Connect>& graf,string kolej){ // wyswietlenie planszy
  int rozmiar=sqrt(graf.getListV().rozmiar()); // wartosc boku planszy
  char znak; // zmienna pomocnicza do oznaczanie wierszy
  cout<<endl<<"    ";
  for(int i=0;i<rozmiar;i++){ // oznaczenia kolumn
    cout<<" "<<i+1<<" ";
    if(i<10)cout<<" ";
  }
  cout<<endl<<"    ";
  for(int i=0;i<rozmiar;i++)cout<<"___ ";
  znak='a';
  for(int i=0;i<rozmiar;i++){
    cout<<endl<<" "<<znak<<" ";
    znak++; // oznaczenie pierwszego wiersza
    cout<<"|";
    for(int j=0;j<rozmiar;j++){
      cout<<" "<<graf[i*rozmiar+j]<<"|";
      if((i==rozmiar-3)&&(j==rozmiar-1))cout<<"    By zamknac wpisz esc"; // wyswietlenie pierwszego komunikatu
      if((i==rozmiar-2)&&(j==rozmiar-1))cout<<"    "<<kolej; // wyswietlenie napisu z wejsca funkcji
    }
    cout<<endl<<"   |";
    for(int j=0;j<rozmiar;j++)cout<<"___|";
  }
}

bool sprawdz(int **tab,int rozm,int ile,int win){ // tab - tablica, ile - ile znakow wygrywa, win - ktorego gracza sprawdzamy
  bool opt1=false; // wygrana w prawo
  bool opt2=false; // wygrana w prawo i dol
  bool opt3=false; // wygrana w dol
  bool opt4=false; // wygrana w lewo i dol
  bool opt5=false; // wygrana w prawo ale na poziomach = rozmiar - ilosc pol do wygranej
  for(int i=0;i<(rozm-ile+1);i++){
    for(int j=0;j<(rozm-ile+1);j++){
      if(tab[j][i]==win){
	opt1=true;
	opt2=true;
	for(int x=0;x<ile;x++){ // przebiegnij dodatkowo tyle pol ile trzeba do wygrania
	  if(tab[j+x][i]!=win)opt1=false; // jezeli w ciagu znakow ktorys nie bedzie nalezal do gracza to zwroc false
	  if(tab[j+x][i+x]!=win)opt2=false; // jezeli w ciagu znakow ktorys nie bedzie nalezal do gracza to zwroc false
	}
	if(opt1)return opt1;
	if(opt2)return opt2;
      }    	      
    }
  }
  for(int i=0;i<(rozm-ile+1);i++){
    for(int j=0;j<rozm;j++){
      if(tab[j][i]==win){
	opt3=true;
	for(int x=0;x<ile;x++){ // przebiegnij dodatkowo tyle pol ile trzeba do wygrania
	  if(tab[j][i+x]!=win)opt3=false; // jezeli w ciagu znakow ktorys nie bedzie nalezal do gracza to zwroc false
	}
	if(opt3)return opt3;
      }    	      
    }
  }
  for(int i=0;i<rozm;i++){
    for(int j=0;j<(rozm-ile+1);j++){
      if(tab[j][i]==win){
	opt5=true;
	for(int x=0;x<ile;x++){ // przebiegnij dodatkowo tyle pol ile trzeba do wygrania
	  if(tab[j+x][i]!=win)opt5=false; // jezeli w ciagu znakow ktorys nie bedzie nalezal do gracza to zwroc false
	}
	if(opt5)return opt5;
      }    	      
    }
  }
  for(int i=0;i<(rozm-ile+1);i++){
    for(int j=ile-1;j<rozm;j++){
      if(tab[j][i]==win){
	opt4=true;
	for(int x=0;x<ile;x++){ // przebiegnij dodatkowo tyle pol ile trzeba do wygrania
	  if(tab[j-x][i+x]!=win)opt4=false; // jezeli w ciagu znakow ktorys nie bedzie nalezal do gracza to zwroc false
	}
	if(opt4)return opt4;
      }    	      
    }
  }      
  return false; // dany gracz nie ma ani jednego pola zapelnionego
}
